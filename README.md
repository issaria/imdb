# Get started #

### Run tests

```
bundle exec rake spec
```

Sqlite3 used for development and test

### See in real

```
bundle exec rails s
```

open http://localhost:3000