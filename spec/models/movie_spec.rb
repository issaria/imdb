require 'rails_helper'

RSpec.describe Movie, type: :model do
  it { should belong_to :user }
  it { should validate_presence_of :user }
  it { should validate_presence_of :name }
  it { should validate_presence_of :description }
  it { should validate_presence_of :stars }
  it { should validate_uniqueness_of :name }

  it { should have_attached_file(:poster) }
  it { should validate_attachment_presence(:poster) }
  it { should validate_attachment_content_type(:poster).
                allowing('image/png', 'image/gif').
                rejecting('text/plain', 'text/xml') }
  it { should validate_attachment_size(:poster).
                less_than(2.megabytes) }
end
