require 'rails_helper'

RSpec.describe User, type: :model do
  it { should have_many :movies }
  it { should validate_presence_of :email }
  it { should validate_uniqueness_of :email }

  it "concats first name and last name as full name" do
    user = build(:user)
    expect(user.full_name).to eq("#{user.first_name} #{user.last_name}")
  end
end
