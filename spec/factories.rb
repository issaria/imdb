FactoryGirl.define do
  sequence :movie_name do |n|
    "Fast & Furious #{n}"
  end

  factory :user do
    sequence(:email) { |n| "test#{n}@example.com" }
    first_name "John"
    last_name "Doe"
    password "nopassword"
  end

  factory :movie do
    user
    sequence(:name) { |n| "Fast & Furious #{n}" }
    description "Deckard Shaw seeks revenge against Dominic Toretto and his family for his comatose brother."
    director "James Wan"
    stars "Vin Diesel, Paul Walker, Dwayne Johnson"
    poster { fixture_file_upload Rails.root.join("spec/fixtures/fast_and_furious.jpg"), "image/jpeg" }
  end

  factory :minions, class: Hash do
    skip_create

    name "Minions"
    stars "Sandra Bullock, Jon Hamm, Michael Keaton"
    director "Kyle Balda, Pierre Coffin"
    company "Illumination Entertainment"
    description "Minions Stuart, Kevin and Bob are recruited by Scarlett Overkill, a super-villain who, alongside her inventor husband Herb, hatches a plot to take over the world."
    poster { fixture_file_upload(Rails.root.join("spec/fixtures/minions.jpg"), "image/jpep") }

    initialize_with { attributes }
  end
end
