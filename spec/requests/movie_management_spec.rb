require 'rails_helper'

RSpec.describe 'Movie management', type: :request do
  let(:user) { create(:user) }
  let(:movie) { build(:minions) }

  before do
    login(user)
  end

  it "creates a movie and redirects to the movie's page" do
    get '/movies/new'
    expect(response).to render_template(:new)

    post '/movies', movie: movie
    expect(response).to redirect_to(assigns(:movie))

    follow_redirect!
    expect(response).to render_template(:show)
    expect(response.body).to include("Movie was successfully created.")
    expect(response.body).to include(movie[:description])
  end

  context "update and destroy existing movie" do
    let!(:movie) { create(:movie, user: user) }
    it "updates a movie and redirect to the movie's page" do
      get "/movies/#{movie.id}/edit"
      expect(response).to render_template(:edit)

      put "/movies/#{movie.id}", movie: { name: "ice age" }
      expect(response).to redirect_to(assigns(:movie))

      follow_redirect!
      expect(response.body).to include("Movie was successfully updated.")
      expect(response.body).to include("ice age")
    end

    it "destroys a movie and redirect to the index page" do
      delete "/movies/#{movie.id}"
      expect(response).to redirect_to(movies_path)

      follow_redirect!
      expect(response).to render_template(:index)
      expect(response.body).to include("Movie was successfully deleted.")
    end
  end
end

def login(user)
  post user_session_path, user: { email: user.email, password: 'nopassword' }
end
