require 'rails_helper'

RSpec.describe WelcomeController, type: :controller do
  describe "Get #index" do
    context "guest user" do
      it "asks for login or signup" do
        get :index
        expect(response).to have_http_status(:success)
        expect(response).to render_template(:index)
      end
    end

    context "logged in" do
      it "redirects to movies path" do
        sign_in create(:user)
        get :index
        expect(response).to redirect_to(movies_path)
      end
    end
  end
end
