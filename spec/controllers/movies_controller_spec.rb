require 'rails_helper'

RSpec.describe MoviesController, type: :controller do
  it { should use_before_action :authenticate_user! }
  it { should permit(:name, :description, :stars, :company, :director, :poster).
       for(:create) }

  let(:user) { create(:user) }
  let!(:movie) { create(:movie, user: user) }

  before do
    sign_in user
  end

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
      expect(assigns(:movies)).to include(movie)
    end
  end

  describe "GET #new" do
    it "returns http success" do
      get :new
      expect(response).to have_http_status(:success)
      expect(assigns(:movie)).to be_new_record
    end
  end

  describe "POST #create" do
    it "creates a movie and redirect to show page" do
      post :create, movie: { name: "Mud", stars: "Matthew McConaughey, Tye Sheridan, Jacob Lofland",
                             description: "Two young boys encounter a fugitive and form a pact to help him evade the vigilantes that are on his trail and to reunite him with his true love.",
                             poster: fixture_file_upload("mud.jpg", "image/jpep")}
      expect(assigns(:movie)).to be_persisted
      expect(assigns(:movie).user).to eq(user)
      expect(response).to redirect_to(movie_path(assigns(:movie)))
    end
  end

  describe "GET #show" do
    it "displays the movie" do
      get :show, id: movie.id
      expect(assigns(:movie)).to eq(movie)
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #edit" do
    it "returns http success" do
      get :edit, id: movie.id
      expect(response).to have_http_status(:success)
      expect(assigns(:movie)).to eq(movie)
    end
  end

  describe "PUT #update" do
    it "updates name of the movie" do
      put :update, id: movie.id, movie: { name: "Seatle sleepless" }
      expect(assigns(:movie).name).to eq("Seatle sleepless")
      expect(response).to redirect_to(movie_path(movie))
    end
  end

  describe "DELETE #destroy" do
    it "deletes the movie and redirects to the index page" do
      delete :destroy, id: movie.id
      expect(assigns(:movie)).to be_destroyed
      expect(flash[:notice]).to be_present
      expect(response).to redirect_to(movies_path)
    end
  end
end
