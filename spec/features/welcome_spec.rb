require 'rails_helper'

include Warden::Test::Helpers
Warden.test_mode!
RSpec.describe 'Welcome page', type: :feature do
  let(:user) { create(:user) }

  scenario "Guest user" do
    visit "/"

    expect(page).to have_text("Login")
    expect(page).to have_text("Signup")
  end

  scenario "Logged in" do
    login_as(user, scope: :user)
    visit "/"

    expect(page).not_to have_text("Login")
    expect(page).not_to have_text("Signup")
    expect(page).to have_text(user.full_name)
    expect(page).to have_text("logout")

    click_link "logout"
    expect(page).to have_text("Login")
    expect(page).to have_text("Signup")
  end
end
