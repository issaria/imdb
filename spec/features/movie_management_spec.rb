require 'rails_helper'

include Warden::Test::Helpers
Warden.test_mode!
RSpec.describe 'Movie management', type: :feature do
  let(:minions) { build(:minions) }
  let(:user) { create(:user) }
  before do
    login_as(user, scope: :user)
  end

  scenario "User check movie list" do
    13.times do |i|
      create(:movie, user: user)
    end
    visit "/"

    expect(page).to have_css("div.pagination")
  end

  scenario "User create a new movie" do
    visit "/"
    click_link "Movies"
    expect(page).to have_text("New")

    click_link "New"
    expect(page).to have_text("Add new movie information")
    fill_in "Name", with: minions[:name]
    fill_in "Director", with: minions[:director]
    fill_in "Description", with: minions[:description]
    fill_in "Stars", with: minions[:stars]
    fill_in "Company", with: minions[:company]
    attach_file "Poster", Rails.root.join("spec/fixtures/minions.jpg")
    click_button "Create"
    expect(page).to have_text("Movie was successfully created.")
    expect(page).to have_text(minions[:name])
  end

  scenario "User edit an existing movie" do
    movie = create(:movie, user: user)
    visit "/movies"
    expect(page).to have_text(movie.name)
    expect(page).to have_xpath("//img[@src=\"#{movie.poster.url(:medium)}\"]")

    click_link movie.name
    expect(page).to have_xpath("//img[@src=\"#{movie.poster.url(:medium)}\"]")
    expect(page).to have_text(movie.description)
    expect(page).to have_text(movie.stars[0])
    expect(page).to have_text(movie.director)
    expect(page).to have_content("edit")

    click_link "edit"
    expect(page).to have_text("Edit movie #{movie.name}")
    fill_in "Name", with: "Interstellar"

    click_button "Update"
    expect(page).to have_text("Interstellar")
    expect(page).to have_text("Movie was successfully updated.")
  end

  scenario "User delete an movie" do
    movie1 = create(:movie, user: user)
    movie2 = create(:movie, user: user)
    visit "/movies"
    expect(page).to have_text(movie1.name)
    expect(page).to have_text(movie2.name)

    click_link movie1.name
    expect(page).to have_text("delete")

    click_link "delete"
    expect(page).not_to have_text(movie1.name)
  end
end
