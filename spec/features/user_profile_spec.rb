require 'rails_helper'

include Warden::Test::Helpers
Warden.test_mode!
RSpec.describe 'Update user profile', type: :feature do
  let(:user) { create(:user) }

  scenario "Update email, first name and last name" do
    login_as(user, scope: :user)
    visit "/"

    click_link user.full_name
    expect(page).to have_text("Edit profile")
    fill_in "Email", with: "test@example.com"
    fill_in "First name", with: "Adam"
    fill_in "Last name", with: "Smith"
    fill_in "Current password", with: "nopassword"

    click_button "Update"
    expect(page).to have_text("Your account has been updated successfully.")
  end
end
