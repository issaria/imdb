class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.integer :user_id
      t.string :name
      t.string :director
      t.string :company
      t.string :category
      t.string :writers
      t.string :stars
      t.datetime :published_at
      t.string :description

      t.timestamps null: false
    end
  end
end
