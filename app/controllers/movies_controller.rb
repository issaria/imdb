class MoviesController < ApplicationController
  before_action :authenticate_user!
  before_action :find_movie, only: [:edit, :update, :show, :destroy]

  def index
    @movies = current_user.movies.paginate(page: params[:page], per_page: 12)
  end

  def new
    @movie = Movie.new
  end

  def create
    @movie = current_user.movies.new(movie_params)
    if @movie.save
      flash[:notice] = "Movie was successfully created."
      redirect_to movie_path(@movie)
    else
      flash[:alert] = "Something is wrong, please check the form."
      render :new
    end
  end

  def show
  end

  def edit
  end


  def update
    if @movie.update_attributes(movie_params)
      flash[:notice] = "Movie was successfully updated."
      redirect_to movie_path(@movie)
    else
      flash[:alert] = "Something is wrong, please check the form."
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @movie.update_attributes(movie_params)
      flash[:notice] = "Movie was successfully updated."
      redirect_to movie_path(@movie)
    else
      flash[:alert] = "Something is wrong, please check the form."
      render :edit
    end
  end

  def destroy
    @movie.destroy
    flash[:notice] = "Movie was successfully deleted."
    redirect_to movies_path
  end

  private
  def find_movie
    @movie = current_user.movies.find(params[:id])
  end

  def movie_params
    params.require(:movie).permit(:name, :description, :poster, :stars, :director, :company)
  end
end
