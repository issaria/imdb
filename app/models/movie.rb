class Movie < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :user, :name, :description, :stars
  validates_uniqueness_of :name

  has_attached_file :poster, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment :poster, presence: true,
    content_type: { content_type: /\Aimage\/.*\Z/ },
    size: {in: 0..2.megabytes}
end
