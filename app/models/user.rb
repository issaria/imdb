class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :movies
  validates_presence_of :email
  validates_uniqueness_of :email

  def full_name
    "%s %s" % [first_name, last_name]
  end
end
